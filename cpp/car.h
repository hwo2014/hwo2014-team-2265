#ifndef HWO_CAR_H
#define HWO_CAR_H

#include <string>
#include <vector>

class dimension
{
	public:
		dimension(double length = 0, double width = 0, double guide_flag_position = 0)
		 : length(length), width(width), guide_flag_position(guide_flag_position)
		{}

		double get_length() { return length; }
		double get_width() { return width; }
		double get_guide_flag_position() { return guide_flag_position; }

		void set_length(double l) { length = l; }
		void set_width(double w) { width = w; }
		void set_guide_flag_position(double g) { guide_flag_position = g; }

	private:
		double length, width;
		double guide_flag_position;
};

class car
{
	public:
		car(const std::string& name = "", const std::string& color = "", 
		    double length = 0, double width = 0, double guide_flag_position = 0)
		 : name(name), color(color), dim(length, width, guide_flag_position)
		{}

		const std::string& get_name() { return name; }
		const std::string& get_color() { return color; }


		dimension& get_dimension() { return dim; }

	private:
		std::string name, color;
		dimension dim;
		
};


typedef std::vector<car*> cars_vector;

#endif

