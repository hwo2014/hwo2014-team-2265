#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : 	action_map {
		{ "join", &game_logic::on_join },

		{ "yourCar", &game_logic::on_your_car },
		{ "gameInit", &game_logic::on_game_init },
		{ "gameStart", &game_logic::on_game_start },
		{ "carPositions", &game_logic::on_car_positions },
		{ "crash", &game_logic::on_crash },
		{ "gameEnd", &game_logic::on_game_end },
		{ "error", &game_logic::on_error }
	},
	pos_ant(0),
	my_car(0),
	cars()
{
}


game_logic::~game_logic()
{
	delete my_car;
	for (size_t i = 0; i < cars.size(); ++i) delete cars[i];
}


game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);

	if (action_it != action_map.end()) {
		return (action_it->second)(this, data);
	}
	else {
		std::cout << "Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}


game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	std::cout << "Your car" << std::endl;

	std::cout << "RAW DATA: ";
	std::cout << data << std::endl;

	my_car = new car(data["name"].as<std::string>(), data["color"].as<std::string>());

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "Joined" << std::endl;

	std::cout << "RAW DATA: ";
	std::cout << data << std::endl;

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	std::cout << "Race initied" << std::endl;

	std::cout << "RAW DATA: ";
	std::cout << data << std::endl;

	get_cars(data["race"]["cars"]);

	return { make_ping() };
}

car* game_logic::get_car(const jsoncons::json& data)
{
	return new car(data["id"]["name"].as<std::string>(), 
		       data["id"]["color"].as<std::string>(), 
		       data["dimensions"]["length"].as<double>(), 
		       data["dimensions"]["width"].as<double>(), 
		       data["dimensions"]["guideFlagPosition"].as<double>());
}

void game_logic::get_cars(const jsoncons::json& data)
{
	for (size_t i = 0; i < data.size(); ++i) {
		std::cout << "=========================================" << std::endl;
		std::cout << "car : " << i << std::endl;
		std::cout << data[i] << std::endl;
		std::cout << "---------------------------- " << std::endl;

		cars.push_back(get_car(data[i]));

		if (cars[i]->get_name() == my_car->get_name() and cars[i]->get_color() == my_car->get_color()) {
			my_car->get_dimension().set_length(cars[i]->get_dimension().get_length());
			my_car->get_dimension().set_width(cars[i]->get_dimension().get_length());
			my_car->get_dimension().set_guide_flag_position(cars[i]->get_dimension().get_guide_flag_position());
		}
	}
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "Race started" << std::endl;

	std::cout << "RAW DATA: ";
	std::cout << data << std::endl;

	
	return { make_ping() };
}


game_logic::msg_vector game_logic::do_action(const jsoncons::json& data, size_t i)
{

	switch (data[i]["piecePosition"]["pieceIndex"].as<int>()) {
		case 36: return { make_throttle(0) };
		case 4: return { make_throttle(0.72)};
	}
	
  	return { make_throttle(0.62) };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	/*
	"piecePosition": {
	      "pieceIndex": 0,
	      "inPieceDistance": 0.0,
	      "lane": {
		"startLaneIndex": 0,
		"endLaneIndex": 0
	      },
	      "lap": 0
	    }
	*/
	std::cout << "RAW DATA: ";
	std::cout << data << std::endl;

	for (size_t i = 0; i < data.size(); ++i) {
		std::cout << "=========================================" << std::endl;
		std::cout << "i : " << i << std::endl;
		std::cout << data[i] << std::endl;
		std::cout << "------------------------------" << std::endl;		
		std::cout << "angle: " << data[i]["angle"] << std::endl;
		std::cout << "piece index: " << data[i]["piecePosition"]["pieceIndex"] << std::endl;
		std::cout << "piece distance: " << data[i]["piecePosition"]["inPieceDistance"] << std::endl;
		std::cout << "piece lane start: " << data[i]["piecePosition"]["lane"]["startLaneIndex"] << std::endl;
		std::cout << "piece lane end: " << data[i]["piecePosition"]["lane"]["endLaneIndex"] << std::endl;
		std::cout << "lap: " << data[i]["piecePosition"]["lap"] << std::endl;				
		std::cout << std::endl;


		if (data[i]["id"]["name"] == my_car->get_name() and data[i]["id"]["color"] == my_car->get_color()) {
			std::cout << "It's my car =)!" << std::endl;
			return do_action(data, i);
		}
	}

  	return { make_throttle(0.62) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "Someone crashed" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
	return { make_ping() };
}


